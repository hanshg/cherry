package com.hanshg.cherry.util.http;

import com.hanshg.cherry.model.SysNews;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URL;
import java.util.List;

/**
 * @ClassName HtmlParseUtil
 * @Description 解析网页
 * @Author 柠檬水
 * @Date 2020/4/9 14:24
 * @Version 1.0
 **/
public class HtmlParseUtil {

    public List<SysNews> parseHtml(String keyword) throws Exception {

        String url = "https://search.jd.com/Search?keyword=" + keyword;
        //抓取今日头条页面资源
        Document document = Jsoup.parse(new URL(url), 30000);
        Element element = document.getElementById("J_goodsList");
        System.out.println(element);
        Elements elements = element.getElementsByTag("li");

        for (Element el : elements) {
            String img = el.getElementsByTag("img").eq(0).attr("source-data-lazy-img");
            System.out.println(img);
        }
        return null;
    }

    public static void main(String[] args) throws Exception {
        new HtmlParseUtil().parseHtml("java");
    }
}
