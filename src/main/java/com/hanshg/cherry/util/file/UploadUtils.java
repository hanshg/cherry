package com.hanshg.cherry.util.file;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @author 柠檬水
 */
@Slf4j
public class UploadUtils {

    public final static String IMG_PATH_PREFIX = "static/upload/imgs";

    public final static String FILE_PATH_PREFIX = "static/file";

    private final static String IMG1_PATH_PREFIX = "/upload/imgs/";

    private final static String FILE1_PATH_PREFIX = "/file/";

    public static File getDirFile(boolean type){

        // 构建上传文件的存放 "文件夹" 路径
        String fileDirPath = "";
        if (type){
            fileDirPath = "src/main/resources/" + IMG_PATH_PREFIX;
        }else {
            fileDirPath = "src/main/resources/" + FILE_PATH_PREFIX;
        }

        File fileDir = new File(fileDirPath);
        if(!fileDir.exists()){
            // 递归生成文件夹
            fileDir.mkdirs();
        }
        return fileDir;
    }


    /**
     * 上传到static本地路径
     */
    public JSONObject toTurn(MultipartFile file, HttpServletRequest request, boolean type) {
        JSONObject res = new JSONObject();
        JSONObject resUrl = new JSONObject();
        String filePath = "";
        if (file.isEmpty()) {
            // 设置错误状态码
            res.put("msg", "上传失败，请选择文件");
            res.put("code", 1);
            return res;
        }
        try {
            //文件名处理
            String originalName = file.getOriginalFilename();
            log.info("-----------文件原始的名字【" + originalName + "】-----------");

            String paramName = "";
            if (type) {
                paramName = request.getParameter("username");
            } else {
                paramName = request.getParameter("title");
            }
            log.info("-----------参数值【" + paramName + "】-----------");

            //随机生成图片名称
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            if (StringUtils.isEmpty(paramName)) {
                paramName = uuid;//自动生成文件名
            }

            String filename = originalName.substring(originalName.lastIndexOf("."),
                    originalName.length());
            String newName = paramName + filename;
            log.info("-----------文件要保存后的新名字【" + newName + "】-----------");

            // 存放上传图片的文件夹
            File fileDir = UploadUtils.getDirFile(type);

            //绝对路径通畅用于  文件服务器
            //String filePath = request.getScheme() + "://" + request.getServerName() + ":"+ request.getServerPort() + "/upload/imgs/" + newName;

            if (type) {
                filePath = IMG1_PATH_PREFIX + newName;
            } else {
                filePath = FILE1_PATH_PREFIX + newName;
            }
            log.info("-----------图片标签访问路径【" + filePath + "】-----------");

            // 构建真实的文件路径
            File newFile = new File(fileDir.getAbsolutePath() + File.separator + newName);
            log.info("-----------绝对路径是相当于当前项目的路径而不是“容器”路径【" + newFile.getAbsolutePath() + "】-----------");

            // 上传图片到 -》 “绝对路径”
            file.transferTo(newFile);

            resUrl.put("src", filePath);
            resUrl.put("fileName", newName);
            res.put("msg", "");
            res.put("code", 0);
            res.put("data", resUrl);
        } catch (IOException e) {
            resUrl.put("src", "");
            res.put("msg", "上传失败！");
            res.put("code", 1);
            res.put("data", resUrl);
            e.printStackTrace();
        } catch (Exception e) {
            resUrl.put("src", "");
            res.put("msg", "上传失败！");
            res.put("code", 1);
            res.put("data", resUrl);
            e.printStackTrace();
        }
        return res;
    }
}
