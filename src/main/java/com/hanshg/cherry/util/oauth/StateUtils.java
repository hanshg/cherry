package com.hanshg.cherry.util.oauth;

import com.hanshg.cherry.base.core.Constants;
import com.hanshg.cherry.util.core.DateUtils;

/**
 * @Author hanshg
 * @Description //TODO
 * @Date 2021/8/16
 * @Param
 * @return
 **/
public class StateUtils {

    public StateUtils() {

    }

    public static String createState() {
        String content = Constants.PWD_MD5 + DateUtils.dateTime();
        byte[] encrypt = AesUtil.encrypt(content, AesUtil.PASSWORD_SECRET_KEY, 16);
        return AesUtil.parseByte2HexStr(encrypt);
    }

    public static boolean checkState(String state) {
        String decrypt = AesUtil.decrypt(AesUtil.parseHexStr2Byte(state), AesUtil.PASSWORD_SECRET_KEY, 16);
        return !decrypt.equals(Constants.PWD_MD5 + DateUtils.dateTime());
    }
}
