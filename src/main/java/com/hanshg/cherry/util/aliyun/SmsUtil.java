package com.hanshg.cherry.util.aliyun;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.hanshg.cherry.config.aliyun.SmsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName SmsUtil
 * @Description 短信工具类
 * @Author 柠檬水
 * @Date 2020/4/18 16:32
 * @Version 1.0
 **/
@Component
public class SmsUtil {

    @Autowired
    private SmsConfig smsConfig;

    /**
     * 发送短信
     * @param mobile 手机号
     * @param param 参数
     * @return
     */
    public String sendSms(String mobile,String param) {

        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", smsConfig.getAccessKeyId(), smsConfig.getAccessKeySecret());
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(SmsStaticUtil.domain);
        request.setSysVersion("2017-05-25");
        request.setSysAction(SmsStaticUtil.SendSms);
        request.setSysReadTimeout(SmsStaticUtil.Timeout);
        request.setSysConnectTimeout(SmsStaticUtil.Timeout);
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", mobile);
        request.putQueryParameter("SignName", smsConfig.getSign_name());
        request.putQueryParameter("TemplateCode", smsConfig.getTemplate_code());
        request.putQueryParameter("TemplateParam",param);
        try {
            CommonResponse response = client.getCommonResponse(request);
            return response.getData();
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 查询发送细节
     *
     * @param mobile 移动
     * @return {@link String}
     */
    public  String querySendDetails(String mobile) {

        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", smsConfig.getAccessKeyId(), smsConfig.getAccessKeySecret());
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(SmsStaticUtil.domain);
        request.setSysVersion("2017-05-25");
        request.setSysAction(SmsStaticUtil.QuerySendDetails);
        request.putQueryParameter("RegionId", "cn-hangzhou");
        try {
            CommonResponse response = client.getCommonResponse(request);
            return response.getData();
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return null;
    }

}
