package com.hanshg.cherry.task.zztuku;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName SignInDto
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/27 10:17
 * @Version 1.0
 **/
@Data
public class SignInDto implements Serializable {

    private String info;

    private String status;

    private String url;
}
