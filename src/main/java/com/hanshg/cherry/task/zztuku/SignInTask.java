package com.hanshg.cherry.task.zztuku;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hanshg.cherry.base.core.Constants;
import com.hanshg.cherry.util.core.DateUtils;
import com.hanshg.cherry.util.http.HttpClientUtil;
import com.hanshg.cherry.util.redis.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @ClassName SignInTask
 * @Description 定时签到：站长图库  使用QQ登录
 * @Author 柠檬水
 * @Date 2020/4/26 22:30
 * @Version 1.0
 **/
@Component
@Slf4j
public class SignInTask {

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 定时签到
     */
    public void signIn() {
        try {
            //获取网页响应数据
            String data = HttpClientUtil.getSingIn(ConstantCode.URL);
            JSONObject jsonObject = JSON.parseObject(data);
            SignInDto signInDto = JSON.toJavaObject(jsonObject, SignInDto.class);
            log.info("签到信息：" + signInDto.toString());

            //保存缓存为30天  0 0 11 ? * *
            redisUtil.set("signIn"+ DateUtils.getDate(),signInDto, Constants.REDIS_SIGN_IN);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
