package com.hanshg.cherry.base.result;

/**
 * 响应代码
 *
 * @author 柠檬水
 * @Description: 返回对应错误消息编码类
 * @date 2020年3月1日
 */
public enum ResponseCode {

    SUCCESS(200, "请求成功"),
    TABLE_SUCCESS(0, "请求成功"),
    FAIL(500, "请求失败，请联系柠檬水小哥哥"),
    PARAMETER_MISSING(600, "参数缺失"),
    UNAUTHORIZED(401, "未授权"),
    FAILBEAN(501,"BEAN验证错误，请联系樱桃小姐姐"),
    FAILTOKEN(502,"拦截器拦截到用户token出错，请联系樱桃小姐姐"),
    FAILEXCEPTION(555,"异常抛出信息"),


    USERNAME_REPEAT(5000100, "用户已存在"),
    PHONE_REPEAT(5000101, "手机已存在"),
    EMAIL_REPEAT(5000102, "邮箱已存在"),
    VERIFY_CODE(550,"验证码填写错误"),

    USER_ROLE_NO_CLEAR(5000201, "该角色存在用户失联，无法删除");

    private Integer code;

    private String message;

    ResponseCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String getMessage(String name) {
        for(ResponseCode item:ResponseCode.values()){
            if (item.name().equals(name)){
                return item.message;
            }
        }
        return null;
    }

    public static Integer getCode(String name) {
        for(ResponseCode item:ResponseCode.values()){
            if (item.name().equals(name)){
                return item.code;
            }
        }
        return null;
    }
}
