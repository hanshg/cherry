package com.hanshg.cherry.base.result;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 根据layui返回格式数据
 *
 * @param <T>
 * @author 柠檬水
 * @date 2020/04/13
 */
@Data
public class Results<T> implements Serializable {

    /**
     * 数据总数量
     */
    @ApiModelProperty(value = "数据总数量",example = "20")
    Integer count;
    /**
     * 相应代码
     */
    @ApiModelProperty(value = "相应代码",example = "20")
    Integer code;
    /**
     * 消息
     */
    String msg;
    /**
     * 返回数据
     */
    List<T> datas;
    /**
     * 返回任意类型
     */
    T data;

    public Results() {
    }

    public Results(Integer code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }

    public Results(Integer code, String msg, T data, Integer count, List<T> datas) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count;
        this.datas = datas;
    }

    /**
     * 无数据传输的 成功返回
     * @param <T>
     * @return
     */
    public static <T> Results<T> success() {
        return new Results<T>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMessage());
    }

    public static <T> Results<T> success(String msg) {
        return new Results<T>(ResponseCode.SUCCESS.getCode(), msg);
    }

    public static <T> Results<T> success(ResponseCode responseCode) {
        return new Results<T>(responseCode.getCode(), responseCode.getMessage());
    }

    /**
     * 单个数据传输的 成功返回
     * @param data
     * @param <T>
     * @return
     */
    public static <T> Results<T> success(T data) {
        return new Results<T>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMessage(), data, 0, null);
    }

    public static <T> Results<T> success(String msg, T data) {
        return new Results<T>(ResponseCode.SUCCESS.getCode(), msg, data, 0, null);
    }

    public static <T> Results<T> success(ResponseCode responseCode, T data) {
        return new Results<T>(responseCode.getCode(), responseCode.getMessage(), data, 0, null);
    }

    /**
     * 分页数据传输的 成功返回
     * @param count
     * @param datas
     * @param <T>
     * @return
     */
    public static <T> Results<T> success(Integer count, List<T> datas) {
        return new Results<T>(ResponseCode.TABLE_SUCCESS.getCode(), ResponseCode.SUCCESS.getMessage(), null, count, datas);
    }

    public static <T> Results<T> success(String msg, Integer count, List<T> datas) {
        return new Results<T>(ResponseCode.TABLE_SUCCESS.getCode(), msg, null, count, datas);
    }

    public static <T> Results<T> success(ResponseCode responseCode, Integer count, List<T> datas) {
        return new Results<T>(responseCode.getCode(), responseCode.getMessage(), null, count, datas);
    }

    /**
     * 无数据传输的 失败返回
     * @param <T>
     * @return
     */
    public static <T> Results<T> failure() {
        return new Results<T>(ResponseCode.FAIL.getCode(), ResponseCode.FAIL.getMessage());
    }

    public static <T> Results<T> failure(ResponseCode responseCode) {
        return new Results<T>(responseCode.getCode(), responseCode.getMessage());
    }

    public static <T> Results<T> failure(Integer code, String msg) {
        return new Results<T>(code, msg);
    }

    public static Results ok() {
        return new Results();
    }
}
