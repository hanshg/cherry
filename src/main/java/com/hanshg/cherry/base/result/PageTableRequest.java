package com.hanshg.cherry.base.result;

import lombok.Data;

import java.io.Serializable;

/**
 * 分页处理类
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Data
public class PageTableRequest implements Serializable {

    private Integer page;
    private Integer limit;
    private Integer offset;

    public void countOffset() {
        if (null == this.page || null == this.limit) {
            this.offset = 0;
        }
        this.offset = (this.page - 1) * limit;
    }
}
