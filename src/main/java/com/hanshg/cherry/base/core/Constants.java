package com.hanshg.cherry.base.core;

/**
 * @ClassName Constants
 * @Description 常量工具类
 * @Author 柠檬水
 * @Date 2020/4/16 20:57
 * @Version 1.0
 **/
public class Constants {

    /**
     * 登录验证会话
     */
    public static final String LOGIN_VERIFY_SESSION = "login_verify";

    /**
     * 登录路径
     */
    public static final String LOGIN_PATH = "/login";

    /**
     * 手机验证码登录请求处理url
     */
    public static final String DEFAULT_LOGIN_PROCESSING_URL_MOBILE = "/sms/login";

    /**
     * 提交
     */
    public static final String SUBMIT_POST = "POST";

    /**
     * JSON
     */
    public static final String LOGIN_TYPE_JSON = "JSON";

    /**
     * 提交获得
     */
    public static final String SUBMIT_GET = "GET";

    /**
     * 允许上传
     */
    public static final String IS_FLAG = "true";

    /**
     * 不允许上传
     */
    public static final String IS_FLAG_FALSE = "false";

    /**
     * 短信验证码前缀
     */
    public static final String CHECK_CODE = "checkCode";

    /**
     * 手机验证码登录验证码表单字段名
     */
    public static final String SMS_CODE = "smsCode";

    /**
     * redis缓存 短信您验证码 失效时间 秒
     */
    public static final long REDIS_SMS_EXPIRE = 60 * 5;

    /**
     * redis缓存 短信验证码发送次数 前缀
     */
    public static final String REDIS_SMS_USER_PREFIX = "checkCount";

    /**
     * redis缓存 短信验证码发送次数限制 失效时间 秒
     */
    public static final long REDIS_SMS_USER_EXPIRE = 60 * 60 * 24;

    /**
     * redis缓存设置签到 超时时间30天
     */
    public static final long REDIS_SIGN_IN = 60 * 60 * 24 * 30;

    /**
     * redis缓存 登录验证码 失效时间 秒
     */
    public static final long SMS_USER_EXPIRE = 60 * 2;

    /**
     * 验证码 数字长度
     */
    public static final int SMS_LENGTH = 6;

    /**
     * 自定义加密措施
     */
    public static final String PWD_MD5 = "cherry_soft";

    /**
     * 手机验证码登录手机号表单字段名
     */
    public static final String SPRING_SECURITY_FORM_MOBILE_KEY = "telephone";

}
