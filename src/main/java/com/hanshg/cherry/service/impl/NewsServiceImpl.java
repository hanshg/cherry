package com.hanshg.cherry.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hanshg.cherry.base.result.JSONResult;
import com.hanshg.cherry.mapper.wx.NewsMapper;
import com.hanshg.cherry.service.wx.NewsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 柠檬水
 */
@Service
@Transactional
public class NewsServiceImpl implements NewsService {

    private final NewsMapper newsMapper;

    public NewsServiceImpl(NewsMapper newsMapper) {
        this.newsMapper = newsMapper;
    }

    @Override
    public JSONResult getNewsByTypeId(Long typeId) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("type_id",typeId);
        return JSONResult.ok(newsMapper.selectList(wrapper));
    }

    @Override
    public JSONResult getNewsById(Long id) {
        return JSONResult.ok(newsMapper.selectById(id));
    }
}
