package com.hanshg.cherry.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.mapper.client.ClientProductMapper;
import com.hanshg.cherry.model.SysClientProduct;
import com.hanshg.cherry.service.client.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 柠檬水
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ClientProductMapper clientProductMapper;

    @Override
    public Results<SysClientProduct> getProductByPage(Integer offset, Integer limit) {
        Page<SysClientProduct> page = new Page<>(offset, limit);
        QueryWrapper<SysClientProduct> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("sort");
        IPage<SysClientProduct> productPage = clientProductMapper.selectPage(page, wrapper);
        return Results.success((int) productPage.getTotal(), productPage.getRecords());
    }

    @Override
    public Results<SysClientProduct> getProductByPage(Integer offset, Integer limit, SysClientProduct clientProduct) {
        Page<SysClientProduct> page = new Page<>(offset, limit);
        QueryWrapper<SysClientProduct> wrapper = new QueryWrapper<>();
        wrapper.like("title", clientProduct.getTitle());
        wrapper.orderByAsc("sort");
        IPage<SysClientProduct> list = clientProductMapper.selectPage(page, wrapper);
        return Results.success((int) list.getTotal(), list.getRecords());
    }

    @Override
    public Results saveProduct(SysClientProduct clientProduct) {
        int i = clientProductMapper.insert(clientProduct);
        if (i > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    @Override
    public Results<SysClientProduct> getProductById(Long id) {
        return Results.success(clientProductMapper.selectById(id));
    }

    @Override
    public Results updateProduct(SysClientProduct clientProduct) {
        int i = clientProductMapper.updateById(clientProduct);
        if (i > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    @Override
    public Results deleteProduct(Long id) {
        int i = clientProductMapper.deleteById(id);
        if (i > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    @Override
    public SysClientProduct getSysClientProductById(Long id) {
        return clientProductMapper.selectById(id);
    }

    @Override
    public Results<SysClientProduct> recycleList() {
        return Results.success(0,clientProductMapper.recycleList());
    }

    @Override
    public Integer deleteRecycle(Long id) {
        return clientProductMapper.deletePeace(id);
    }

    @Override
    public Results reduceProduct(SysClientProduct clientProduct) {
        int i = clientProductMapper.reduceProduct(clientProduct.getId());
        if (i > 0) {
            return Results.success();
        }
        return Results.failure();
    }

    @Override
    public List<SysClientProduct> allProduct() {
        QueryWrapper<SysClientProduct> wrapper = new QueryWrapper<>();
        wrapper.eq("status", 1);
        wrapper.orderByAsc("sort");
        return clientProductMapper.selectList(wrapper);
    }
}
