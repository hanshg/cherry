package com.hanshg.cherry.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hanshg.cherry.mapper.log.ErrorLogMapper;
import com.hanshg.cherry.model.SysErrorLog;
import com.hanshg.cherry.service.log.ErrorLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @ClassName ErrorLogServiceImpl
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/21 21:20
 * @Version 1.0
 **/
@Service
@Transactional
public class ErrorLogServiceImpl extends ServiceImpl<ErrorLogMapper,SysErrorLog> implements ErrorLogService {

}
