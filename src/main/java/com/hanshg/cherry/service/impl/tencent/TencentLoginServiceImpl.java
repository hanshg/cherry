package com.hanshg.cherry.service.impl.tencent;

import com.alibaba.fastjson.JSON;
import com.hanshg.cherry.base.core.Constants;
import com.hanshg.cherry.dto.oauth.AccessTokenWX;
import com.hanshg.cherry.dto.oauth.HttpParame;
import com.hanshg.cherry.dto.oauth.WechatUserUnionID;
import com.hanshg.cherry.service.oauth.TencentLoginService;
import com.hanshg.cherry.util.core.DateUtils;
import com.hanshg.cherry.util.oauth.AesUtil;
import com.hanshg.cherry.util.oauth.HttpClientUtils;
import com.hanshg.cherry.util.oauth.PropertiesUtil;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Service
public class TencentLoginServiceImpl implements TencentLoginService {

    /**
     * 微信登录地址
     *
     * @return
     * @author hanshg
     * @date
     */
    @Override
    public String wxLoginUrl() {
        //
        String content = Constants.PWD_MD5 + DateUtils.dateTime();
        byte[] encrypt = AesUtil.encrypt(content, AesUtil.PASSWORD_SECRET_KEY, 16);
        String parseByte2HexStr = AesUtil.parseByte2HexStr(encrypt);
        String url = HttpParame.AUTHORIZATION_URL;
        url = url.replaceAll("APPID", PropertiesUtil.getValue(HttpParame.APPID).trim());
        try {
            url = url.replaceAll("REDIRECT_URI", URLEncoder.encode(
                    PropertiesUtil.getValue(HttpParame.REDIRECT_URI).trim(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        url = url.replaceAll("SCOPE", "snsapi_login");
        url = url.replace("STATE", parseByte2HexStr);    //加密state进行验证 回调地址当天有效 防止恶意攻击
        return url;
    }

    /**
     * 用户授权后获取用户唯一标识
     *
     * @param code
     * @return
     */
    @Override
    public AccessTokenWX getAccessToken(String code) {
        String accessTokenUrl = HttpParame.ACCESS_TOKEN_URL;
        accessTokenUrl = accessTokenUrl.replaceAll("APPID", PropertiesUtil.getValue(HttpParame.APPID).trim());
        accessTokenUrl = accessTokenUrl.replaceAll("SECRET", PropertiesUtil.getValue(HttpParame.SECRET).trim());
        accessTokenUrl = accessTokenUrl.replaceAll("CODE", code);
        String responseContent = HttpClientUtils.getInstance().sendHttpGet(accessTokenUrl);
        if (responseContent == null || responseContent == "") {
            return null;
        }
        AccessTokenWX accessTokenWX = JSON.parseObject(responseContent, AccessTokenWX.class);
        return accessTokenWX;
    }

    /**
     * 获取用户统一标识。针对一个微信开放平台帐号下的应用，
     * 同一用户的unionid在多个应用中是唯一的。
     * 此方法不牵扯到多个应用时候可以不用。
     * 此处用到只是为了获取微信扫码用户的省份城市(此信息获取的只是微信用户所填的城市省份，
     * 并不是用户的实时位置信息，如果用户未填写是获取不到的。)
     *
     * @param access_token
     * @param openid
     * @return
     */
    @Override
    public WechatUserUnionID getUserUnionID(String access_token, String openid) {
        String unionIDUrl = HttpParame.GET_UNIONID_URL;
        unionIDUrl = unionIDUrl.replace("ACCESS_TOKEN", access_token);
        unionIDUrl = unionIDUrl.replace("OPENID", openid);
        String responseContent = HttpClientUtils.getInstance().sendHttpGet(unionIDUrl);
        System.out.println("responseContent:" + responseContent);
        if (responseContent == null || responseContent == "") {
            return null;
        }
        WechatUserUnionID wechatUserUnionID = JSON.parseObject(responseContent, WechatUserUnionID.class);
        return wechatUserUnionID;
    }
}
