package com.hanshg.cherry.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hanshg.cherry.mapper.file.OssMapper;
import com.hanshg.cherry.model.SysOss;
import com.hanshg.cherry.service.aliyun.OssService;
import com.hanshg.cherry.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * oss服务impl
 *
 * @ClassName OssServiceImpl
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/16 10:10
 * @Version 1.0
 **/
@Service
@Transactional
@Slf4j
public class OssServiceImpl extends ServiceImpl<OssMapper, SysOss> implements OssService {

    @Autowired
    private UserService userService;

    /**
     * 得到系统oss
     *
     * @return {@link SysOss}
     */
    @Override
    public SysOss getSysOss() {
        QueryWrapper<SysOss> wrapper = new QueryWrapper<>();
        wrapper.eq("operation", userService.getLoginUser().getUsername());
        SysOss sysOss = super.getOne(wrapper);
        return sysOss;
    }
}
