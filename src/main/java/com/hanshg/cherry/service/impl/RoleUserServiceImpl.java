package com.hanshg.cherry.service.impl;

import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.mapper.RoleUserMapper;
import com.hanshg.cherry.model.SysRoleUser;
import com.hanshg.cherry.service.RoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 柠檬水
 */
@Service
@Transactional
public class RoleUserServiceImpl implements RoleUserService {

    @Autowired
    private RoleUserMapper roleUserMapper;

    @Override
    public Results getSysRoleUserByUserId(Long userId) {
        SysRoleUser sysRoleUser = roleUserMapper.getSysRoleUserByUserId(userId);
        if (sysRoleUser != null){
            return Results.success(sysRoleUser);
        }else {
            return Results.success();
        }
    }


}
