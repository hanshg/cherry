package com.hanshg.cherry.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.mapper.PermissionMapper;
import com.hanshg.cherry.model.SysPermission;
import com.hanshg.cherry.service.PermissionService;
import com.hanshg.cherry.util.core.TreeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 柠檬水
 */
@Service
@Transactional
@Slf4j
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public Results<JSONArray> listAllPermission() {
        log.info(getClass().getName() + ".listAllPermission()");
        List<SysPermission> datas = permissionMapper.findAll();
        JSONArray array = new JSONArray();
        log.info(getClass().getName() + ".setPermissionsTree(?,?,?)");
        TreeUtils.setPermissionsTree(0L, datas, array);
        return Results.success(array);
    }

    @Override
    public Results<SysPermission> listByRoleId(Long roleId) {
        return Results.success(0, permissionMapper.listByRoleId(roleId));
    }

    @Override
    public Results<SysPermission> getMenuAll() {
        return Results.success(0, permissionMapper.findAll());
    }

    @Override
    public Results save(SysPermission sysPermission) {
        return (permissionMapper.save(sysPermission) > 0) ? Results.success() : Results.failure();
    }

    @Override
    public SysPermission getSysPermissionById(Long id) {
        return permissionMapper.getSysPermissionById(id);
    }

    @Override
    public Results updateSysPermission(SysPermission sysPermission) {
        return (permissionMapper.updatePermission(sysPermission) > 0) ? Results.success() : Results.failure();
    }

    @Override
    public Results delete(Long id) {
        permissionMapper.deletePermissionById(id);
        permissionMapper.deleteByParentId(id);
        return Results.success();
    }

    @Override
    public List<SysPermission> getMenu() {
        return permissionMapper.findAll();
    }

    @Override
    public Results getMenu(Long userId) {
        List<SysPermission> datas = permissionMapper.listByUserId(userId);
        datas = datas.stream().filter(p -> p.getType().equals(1)).collect(Collectors.toList());
        JSONArray array = new JSONArray();
        log.info(getClass().getName() + ".setPermissionsTree(?,?,?)");
        TreeUtils.setPermissionsTree(0L, datas, array);
        return Results.success(array);
    }

}
