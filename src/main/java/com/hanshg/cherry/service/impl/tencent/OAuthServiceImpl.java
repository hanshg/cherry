package com.hanshg.cherry.service.impl.tencent;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.pinyin.PinyinUtil;
import com.hanshg.cherry.dto.UserDto;
import com.hanshg.cherry.dto.oauth.WechatUserUnionID;
import com.hanshg.cherry.model.SysUser;
import com.hanshg.cherry.service.UserService;
import com.hanshg.cherry.service.oauth.OAuthService;
import com.qq.connect.javabeans.qzone.UserInfoBean;
import lombok.extern.slf4j.Slf4j;
import me.zhyd.oauth.model.AuthUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @Author hanshg
 * @Description //TODO
 * @Date 2021/8/12
 * @Param
 * @return
 **/
@Service
@Slf4j
public class OAuthServiceImpl implements OAuthService {

    @Autowired
    private UserService userService;

    @Override
    public SysUser qqLogin(UserInfoBean userInfoBean, String openID) {
        SysUser loginUser = userService.getUserByOpenId(openID);
        if (loginUser == null) {
            // 获取成功 拿到用户信息
            UserDto userDto = new UserDto();
            userDto.setOpenid(openID);
            userDto.setNickname(userInfoBean.getNickname());
            userDto.setUsername(PinyinUtil.getPinyin(userInfoBean.getNickname(), StrUtil.EMPTY));
            userDto.setSex(userInfoBean.getGender() == "男" ? 1 : 0);
            userDto.setPhoto(userInfoBean.getAvatar().getAvatarURL100());
            userDto.setPassword(new BCryptPasswordEncoder().encode("123456"));
            userDto.setStatus(1);//有效

            //保存用户到数据库
            userService.save(userDto, 10L);

            log.info("QQ登录成功后用户信息：" + userDto);
            return userDto;
        }
        loginUser.setNickname(userInfoBean.getNickname());
        loginUser.setSex(userInfoBean.getGender() == "男" ? 1 : 0);
        loginUser.setPhoto(userInfoBean.getAvatar().getAvatarURL100());
        userService.updateUser(loginUser);
        return loginUser;
    }

    @Override
    public SysUser wxLogin(WechatUserUnionID userUnionID, String openID) {
        SysUser loginUser = userService.getUserByOpenId(openID);
        if (loginUser == null) {
            // 获取成功 拿到用户信息
            UserDto userDto = new UserDto();
            userDto.setOpenid(openID);
            userDto.setNickname(userUnionID.getNickname());
            userDto.setUsername(PinyinUtil.getPinyin(userUnionID.getNickname(), StrUtil.EMPTY));
            userDto.setSex(userUnionID.getSex() == 1 ? 1 : 0); //1男 0女
            userDto.setPhoto(userUnionID.getHeadimgurl());
            userDto.setPassword(new BCryptPasswordEncoder().encode("123456"));
            userDto.setStatus(1);//有效

            //保存用户到数据库
            userService.save(userDto, 10L);

            log.info("微信登录成功后用户信息：" + userDto);
            return userDto;
        }
        loginUser.setNickname(userUnionID.getNickname());
        loginUser.setSex(userUnionID.getSex() == 1 ? 1 : 0);
        loginUser.setPhoto(userUnionID.getHeadimgurl());
        userService.updateUser(loginUser);
        return loginUser;
    }

    @Override
    public SysUser login(AuthUser authUser) {
        SysUser loginUser = userService.getUserByOpenId(authUser.getUuid());
        if (loginUser == null) {
            // 获取成功 拿到用户信息
            UserDto userDto = new UserDto();
            userDto.setOpenid(authUser.getUuid());
            userDto.setNickname(authUser.getNickname());
            userDto.setUsername(PinyinUtil.getPinyin(authUser.getUsername(), StrUtil.EMPTY));
            userDto.setSex(Integer.valueOf(authUser.getGender().getCode())); //1男 0女
            userDto.setPhoto(authUser.getAvatar());
            userDto.setPassword(new BCryptPasswordEncoder().encode("123456"));
            userDto.setStatus(1);//有效

            //保存用户到数据库
            userService.save(userDto, 10L);
            return userDto;
        }
        loginUser.setNickname(authUser.getNickname());
        loginUser.setSex(Integer.valueOf(authUser.getGender().getCode()));
        loginUser.setPhoto(authUser.getAvatar());
        userService.updateUser(loginUser);
        return loginUser;
    }
}
