package com.hanshg.cherry.service.crawler;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hanshg.cherry.mapper.crawler.CrawlerMapper;
import com.hanshg.cherry.model.SysCrawler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ClassName CrawlerServiceImpl
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/23 14:45
 * @Version 1.0
 **/
@Service
@Transactional
@Slf4j
public class CrawlerServiceImpl extends ServiceImpl<CrawlerMapper, SysCrawler> implements CrawlerService {

    @Override
    public boolean saveCrawler(SysCrawler crawler) {
        QueryWrapper<SysCrawler> wrapper = new QueryWrapper<>();
        wrapper.eq("url", crawler.getUrl());
        wrapper.eq("time", crawler.getTime());
        List<SysCrawler> crawlers = list(wrapper);
        if (crawlers.size() == 0) {
            return saveOrUpdate(crawler);
        }
        return false;
    }
}
