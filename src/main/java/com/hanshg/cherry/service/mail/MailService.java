package com.hanshg.cherry.service.mail;

import java.util.List;
import java.util.Map;

/**
 * @author 柠檬水
 */
public interface MailService {

    /**
     * 发送文本
     *
     * @param subject     主题
     * @param content     内容
     * @param toWho       需要发送的人
     * @param ccPeoples   需要抄送的人
     * @param bccPeoples  需要密送的人
     * @param attachments 需要附带的附件
     * @param isNet 判断是否为网络资源作为附件
     */
    void sendSimpleTextMailActual(String subject, String content, String[] toWho, String[] ccPeoples, String[] bccPeoples, List<Map<String, String>> attachments, boolean isNet);

}
