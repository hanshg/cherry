package com.hanshg.cherry.service.log;

import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.model.SysLog;

public interface LogService {

    Results<SysLog> getAllLogByPage(Integer type,Integer typeMenu,Integer page, Integer limit);

    Results<SysLog> findLogByUsernameByPage(String username,Integer type, Integer page, Integer limit);

    Integer saveLog(SysLog sysLog);

}
