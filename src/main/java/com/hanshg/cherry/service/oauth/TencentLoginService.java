package com.hanshg.cherry.service.oauth;

import com.hanshg.cherry.dto.oauth.AccessTokenWX;
import com.hanshg.cherry.dto.oauth.WechatUserUnionID;

/**
 * 登录接口
 */
public interface TencentLoginService {

    /**
     * 微信登录地址
     *
     * @return
     */
    String wxLoginUrl();

    /**
     * 用户授权后获取用户唯一标识
     *
     * @param code
     * @return
     */
    AccessTokenWX getAccessToken(String code);

    /**
     * 获取用户统一标识。针对一个微信开放平台帐号下的应用，
     * 同一用户的unionid在多个应用中是唯一的。
     * 此方法不牵扯到多个应用时候可以不用。
     * 此处用到只是为了获取微信扫码用户的省份城市(此信息获取的只是微信用户所填的城市省份，
     * 并不是用户的实时位置信息，如果用户未填写是获取不到的。)
     *
     * @return
     */
    WechatUserUnionID getUserUnionID(String access_token, String openid);
}
