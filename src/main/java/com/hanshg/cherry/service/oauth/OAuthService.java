package com.hanshg.cherry.service.oauth;

import com.hanshg.cherry.dto.oauth.WechatUserUnionID;
import com.hanshg.cherry.model.SysUser;
import com.qq.connect.javabeans.qzone.UserInfoBean;
import me.zhyd.oauth.model.AuthUser;

/**
 * @author hanshg
 * @date 2021/8/12
 */
public interface OAuthService {

    SysUser qqLogin(UserInfoBean userInfoBean, String openID);

    SysUser wxLogin(WechatUserUnionID userUnionID, String openID);

    SysUser login(AuthUser authUser);
}
