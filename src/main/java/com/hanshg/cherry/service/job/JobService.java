package com.hanshg.cherry.service.job;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.model.SysJob;

/**
 * 工作服务
 *
 * @author Administrator
 * @date 2020/04/20
 */
public interface JobService extends IService<SysJob> {

    Results<SysJob> saveJob(SysJob sysJob);

    Results<SysJob> updateJob(SysJob sysJob);

    Results<SysJob> deleteJob(SysJob sysJob);
}
