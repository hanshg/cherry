package com.hanshg.cherry.service;

import com.alibaba.fastjson.JSONArray;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.model.SysPermission;

import java.util.List;

/**
 * @author 柠檬水
 */
public interface PermissionService {

    Results<JSONArray> listAllPermission();

    Results<SysPermission> listByRoleId(Long id);

    Results<SysPermission> getMenuAll();

    Results<SysPermission> save(SysPermission sysPermission);

    SysPermission getSysPermissionById(Long id);

    Results  updateSysPermission(SysPermission sysPermission);

    Results delete(Long id);

    List<SysPermission> getMenu();

    Results<SysPermission> getMenu(Long userId);
}
