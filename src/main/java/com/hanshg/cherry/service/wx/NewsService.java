package com.hanshg.cherry.service.wx;

import com.hanshg.cherry.base.result.JSONResult;

/**
 * @author 柠檬水
 */
public interface NewsService {

   /**
    * 根据新闻类型获取资讯列表
    * @param typeId
    * @return
    */
   JSONResult getNewsByTypeId(Long typeId);

   /**
    * 根据新闻主键获取新闻信息
    * @param id
    * @return
    */
   JSONResult getNewsById(Long id);

}

