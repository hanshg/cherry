package com.hanshg.cherry.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 自动填充处理器
 * @author 柠檬水
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        boolean hasSetter = metaObject.hasSetter("createTime");
        //判断实体类是否存在createTime属性
        if (hasSetter){
            log.info("start insertTime fill ...."+new Date());
            this.strictInsertFill(metaObject, "createTime", Date.class, new Date());
        }
        boolean setter = metaObject.hasSetter("updateTime");
        if (hasSetter){
            log.info("start updateTime fill ...."+new Date());
            this.strictUpdateFill(metaObject, "updateTime", Date.class, new Date());
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        //判断实体类是否存在createTime属性
        boolean hasSetter = metaObject.hasSetter("updateTime");
        if (hasSetter){
            log.info("start updateTime fill ...."+new Date());
            this.strictUpdateFill(metaObject, "updateTime", Date.class, new Date());
        }

    }

}
