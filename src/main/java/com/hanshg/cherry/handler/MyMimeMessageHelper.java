package com.hanshg.cherry.handler;

import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.Assert;

import javax.activation.DataHandler;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.UnsupportedEncodingException;
import java.net.URL;

/**
 * @ClassName MyMimeMessageHelper
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/10 11:42
 * @Version 1.0
 **/
public class MyMimeMessageHelper extends MimeMessageHelper {

    /**
     * Create a new MimeMessageHelper for the given MimeMessage,
     * assuming a simple text message (no multipart content,
     * i.e. no alternative texts and no inline elements or attachments).
     * <p>The character encoding for the message will be taken from
     * the passed-in MimeMessage object, if carried there. Else,
     * JavaMail's default encoding will be used.
     *
     * @param mimeMessage the mime message to work on
     * @see #MyMimeMessageHelper(MimeMessage, boolean)
     * @see #getDefaultEncoding(MimeMessage)
     */
    public MyMimeMessageHelper(MimeMessage mimeMessage) {
        super(mimeMessage);
    }

    /**
     * Create a new MimeMessageHelper for the given MimeMessage,
     * assuming a simple text message (no multipart content,
     * i.e. no alternative texts and no inline elements or attachments).
     *
     * @param mimeMessage the mime message to work on
     * @param encoding    the character encoding to use for the message
     * @see #MyMimeMessageHelper(MimeMessage, boolean)
     */
    public MyMimeMessageHelper(MimeMessage mimeMessage, String encoding) {
        super(mimeMessage, encoding);
    }

    /**
     * Create a new MimeMessageHelper for the given MimeMessage,
     * in multipart mode (supporting alternative texts, inline
     * elements and attachments) if requested.
     * <p>Consider using the MimeMessageHelper constructor that
     * takes a multipartMode argument to choose a specific multipart
     * mode other than MULTIPART_MODE_MIXED_RELATED.
     * <p>The character encoding for the message will be taken from
     * the passed-in MimeMessage object, if carried there. Else,
     * JavaMail's default encoding will be used.
     *
     * @param mimeMessage the mime message to work on
     * @param multipart   whether to create a multipart message that
     *                    supports alternative texts, inline elements and attachments
     *                    (corresponds to MULTIPART_MODE_MIXED_RELATED)
     * @throws MessagingException if multipart creation failed
     * @see #MyMimeMessageHelper(MimeMessage, int)
     * @see #getDefaultEncoding(MimeMessage)
     */
    public MyMimeMessageHelper(MimeMessage mimeMessage, boolean multipart) throws MessagingException {
        super(mimeMessage, multipart);
    }

    /**
     * Create a new MimeMessageHelper for the given MimeMessage,
     * in multipart mode (supporting alternative texts, inline
     * elements and attachments) if requested.
     * <p>Consider using the MimeMessageHelper constructor that
     * takes a multipartMode argument to choose a specific multipart
     * mode other than MULTIPART_MODE_MIXED_RELATED.
     *
     * @param mimeMessage the mime message to work on
     * @param multipart   whether to create a multipart message that
     *                    supports alternative texts, inline elements and attachments
     *                    (corresponds to MULTIPART_MODE_MIXED_RELATED)
     * @param encoding    the character encoding to use for the message
     * @throws MessagingException if multipart creation failed
     * @see #MyMimeMessageHelper(MimeMessage, int, String)
     */
    public MyMimeMessageHelper(MimeMessage mimeMessage, boolean multipart, String encoding) throws MessagingException {
        super(mimeMessage, multipart, encoding);
    }

    /**
     * Create a new MimeMessageHelper for the given MimeMessage,
     * in multipart mode (supporting alternative texts, inline
     * elements and attachments) if requested.
     * <p>The character encoding for the message will be taken from
     * the passed-in MimeMessage object, if carried there. Else,
     * JavaMail's default encoding will be used.
     *
     * @param mimeMessage   the mime message to work on
     * @param multipartMode which kind of multipart message to create
     *                      (MIXED, RELATED, MIXED_RELATED, or NO)
     * @throws MessagingException if multipart creation failed
     * @see #MULTIPART_MODE_NO
     * @see #MULTIPART_MODE_MIXED
     * @see #MULTIPART_MODE_RELATED
     * @see #MULTIPART_MODE_MIXED_RELATED
     * @see #getDefaultEncoding(MimeMessage)
     */
    public MyMimeMessageHelper(MimeMessage mimeMessage, int multipartMode) throws MessagingException {
        super(mimeMessage, multipartMode);
    }

    /**
     * Create a new MimeMessageHelper for the given MimeMessage,
     * in multipart mode (supporting alternative texts, inline
     * elements and attachments) if requested.
     *
     * @param mimeMessage   the mime message to work on
     * @param multipartMode which kind of multipart message to create
     *                      (MIXED, RELATED, MIXED_RELATED, or NO)
     * @param encoding      the character encoding to use for the message
     * @throws MessagingException if multipart creation failed
     * @see #MULTIPART_MODE_NO
     * @see #MULTIPART_MODE_MIXED
     * @see #MULTIPART_MODE_RELATED
     * @see #MULTIPART_MODE_MIXED_RELATED
     */
    public MyMimeMessageHelper(MimeMessage mimeMessage, int multipartMode, String encoding) throws MessagingException {
        super(mimeMessage, multipartMode, encoding);
    }

    /**
     * Add an attachment to the MimeMessage, taking the content from a
     * {@code javax.activation.DataSource}.
     * <p>Note that the InputStream returned by the DataSource implementation
     * needs to be a <i>fresh one on each call</i>, as JavaMail will invoke
     * {@code getInputStream()} multiple times.
     * @param attachmentFilename the name of the attachment as it will
     * appear in the mail (the content type will be determined by this)
     * @param url the {@code javax.activation.DataSource} to take
     * the content from, determining the InputStream and the content type
     * @throws MessagingException in case of errors
     * @see #addAttachment(String, org.springframework.core.io.InputStreamSource)
     * @see #addAttachment(String, java.io.File)
     */
    public void addAttachment(String attachmentFilename, URL url) throws MessagingException {
        Assert.notNull(attachmentFilename, "Attachment filename must not be null");
        Assert.notNull(url, "url must not be null");
        try {
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setDisposition(MimeBodyPart.ATTACHMENT);
            mimeBodyPart.setFileName(MimeUtility.encodeText(attachmentFilename));
            mimeBodyPart.setDataHandler(new DataHandler(url));
            getRootMimeMultipart().addBodyPart(mimeBodyPart);
        }
        catch (UnsupportedEncodingException ex) {
            throw new MessagingException("Failed to encode attachment filename", ex);
        }
    }
}
