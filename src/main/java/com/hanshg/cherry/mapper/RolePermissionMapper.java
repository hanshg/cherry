package com.hanshg.cherry.mapper;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.RolePermission;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色权限映射器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Mapper
public interface RolePermissionMapper extends MyBaseMapper<RolePermission> {

    /**
     * 保存
     *
     * @param id            id
     * @param permissionIds ids允许
     * @return int
     */
    int save(@Param("roleId")Long id, @Param("permissionIds") List<Long> permissionIds);

    /**
     * 删除角色权限
     *
     * @param roleId 角色id
     * @return int
     */
    @Delete("delete from sys_role_permission where roleId = #{roleId}")
    int deleteRolePermission(Long roleId);
}
