package com.hanshg.cherry.mapper;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysUser;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 用户映射器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Mapper
public interface UserMapper extends MyBaseMapper<SysUser> {

    /**
     * 获取用户
     *
     * @param username 用户名
     * @return {@link SysUser}
     */
    @Select("select * from sys_user t where t.username = #{username}")
    SysUser getUser(String username);

    /**
     * 得到用户的id
     *
     * @param id id
     * @return {@link SysUser}
     */
    @Select("select * from sys_user t where t.id = #{id}")
    SysUser getUserById(Long id);

    /**
     * 删除用户
     *
     * @param id id
     * @return int
     */
    @Delete("delete from sys_user where id = #{id}")
    int deleteUser(Long id);

    /**
     * 保存用户
     *
     * @param sysUser 系统用户
     * @return int
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_user(username,password,sex,age,birthday,nickname,photo,telephone,email,status,openid,create_time,update_time) " +
            "values(#{username},#{password},#{sex},#{age},#{birthday},#{nickname},#{photo},#{telephone},#{email},#{status},#{openid},now(),now())")
    int saveUser(SysUser sysUser);

    /**
     * 更新用户
     *
     * @param sysUser 系统用户
     * @return int
     */
    int updateUser(SysUser sysUser);

    /**
     * 得到所有用户的页面
     *
     * @param page  页面
     * @param limit 限制
     * @return {@link List<SysUser>}
     */
    @Select("select * from sys_user t order by t.id limit #{page},#{limit} ")
    List<SysUser> getAllUserByPage(@Param("page") Integer page, @Param("limit") Integer limit);

    /**
     * 数用户
     *
     * @return {@link Integer}
     */
    @Select("select count(*) from sys_user t")
    Integer countUser();

    /**
     * 获取用户通过电话
     *
     * @param phone 电话
     * @return {@link SysUser}
     */
    @Select("select * from sys_user t where t.telephone = #{phone}")
    SysUser getUserByPhone(String phone);

    /**
     * 获取用户通过电话
     *
     * @param openid 电话
     * @return {@link SysUser}
     */
    @Select("select * from sys_user t where t.openid = #{openid}")
    SysUser getUserByOpenId(String openid);

    /**
     * 让用户通过电子邮件
     *
     * @param email 电子邮件
     * @return {@link SysUser}
     */
    @Select("select * from sys_user t where t.email = #{email}")
    SysUser getUserByEmail(String email);

    /**
     * 找到用户的用户名
     *
     * @param username 用户名
     * @return {@link Integer}
     */
    @Select("select count(*) from sys_user t where t.username like '%${username}%'")
    Integer findUserByUsername(@Param("username") String username);

    /**
     * 找到用户通过用户名页面
     *
     * @param username 用户名
     * @param page     页面
     * @param limit    限制
     * @return {@link List<SysUser>}
     */
    @Select("select * from sys_user t where t.username like '%${username}%' limit #{page},#{limit}")
    List<SysUser> findUserByUsernameByPage(@Param("username")String username,@Param("page") Integer page,@Param("limit") Integer limit);

    /**
     * 更改密码
     *
     * @param id       id
     * @param password 密码
     * @return int
     */
    @Update("update sys_user t set t.password = #{password} where t.id = #{id}")
    int changePassword(Long id, String password);
}
