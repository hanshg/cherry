package com.hanshg.cherry.mapper.file;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysMailInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 邮件信息映射器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Mapper
public interface MailInfoMapper extends MyBaseMapper<SysMailInfo> {

    /**
     * 回收列表
     *
     * @param operation 操作
     * @return {@link List<SysMailInfo>}
     */
    @Select("select * from sys_mail_info t where t.deleted = 1 and t.operation = #{operation}")
    List<SysMailInfo> recycleList(String operation);

    /**
     * 删除和平
     *
     * @param id id
     * @return {@link Integer}
     */
    @Delete("delete from sys_mail_info t where t.id = #{id}")
    Integer deletePeace(Long id);

    /**
     * 减少产品
     *
     * @param id id
     * @return {@link Integer}
     */
    @Update("update sys_mail_info t set t.deleted = 0 where t.id = #{id}")
    Integer reduceProduct(Long id);
}
