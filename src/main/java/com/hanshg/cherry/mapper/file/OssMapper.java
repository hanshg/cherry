package com.hanshg.cherry.mapper.file;

import com.hanshg.cherry.mapper.base.MyBaseMapper;
import com.hanshg.cherry.model.SysOss;
import org.apache.ibatis.annotations.Mapper;

/**
 * oss映射器
 *
 * @ClassName OssMapper
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/16 10:01
 * @Version 1.0
 **/
@Mapper
public interface OssMapper extends MyBaseMapper<SysOss> {

}
