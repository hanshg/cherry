package com.hanshg.cherry.model;


import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 柠檬水
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "菜单表")
public class SysPermission extends BaseEntity<Long> {

  @ApiModelProperty(value = "父Id",example = "1")
  private Long parentId;
  private String name;
  private String css;
  private String href;
  @ApiModelProperty(value = "类型",example = "1")
  private Integer type;
  private String permission;
  @ApiModelProperty(value = "排序",example = "1")
  private Integer sort;

  private List<SysPermission> child;
}
