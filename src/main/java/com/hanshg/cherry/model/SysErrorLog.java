package com.hanshg.cherry.model;

import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @ClassName SysErrorLog
 * @Description 错误日志表
 * @Author 柠檬水
 * @Date 2020/4/21 21:12
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "异常日志信息")
public class SysErrorLog extends BaseEntity<Long> {

    private String className;

    private String methodName;

    private String exceptionName;

    private String errMsg;

    private String stackTrace;

}
