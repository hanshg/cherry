package com.hanshg.cherry.model;

import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author 柠檬水
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "邮件配置表")
public class SysMail extends BaseEntity<Long> {

	private String host;
	@ApiModelProperty(value = "端口",example = "465")
	private Integer port;
	private String username;
	private String password;
	private String nickname;
	private String protocol;
	private String operation;
	private String encoding;
	@ApiModelProperty(value = "超时时间",example = "30000")
	private Integer timeout;

	public SysMail() {
		this.port = 465;
		this.nickname = "柠檬水";
		this.protocol = "stmp";
		this.encoding = "UTF-8";
		this.timeout = 30000;
	}
}
