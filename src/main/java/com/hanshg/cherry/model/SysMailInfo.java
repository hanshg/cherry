package com.hanshg.cherry.model;

import com.hanshg.cherry.model.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * (SysMailInfo)表实体类
 *
 * @author 柠檬水
 * @since 2020-03-31 20:31:55
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SysMailInfo extends BaseEntity<Long> {

    private String address;

    private String title;

    private String annex;

    private String fileName;

    private String cc;

    private String bcc;

    private String content;

    private String sizeValue;

    private String operation;

}