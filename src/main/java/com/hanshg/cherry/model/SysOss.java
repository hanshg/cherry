package com.hanshg.cherry.model;

import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * sys oss
 *
 * @author 柠檬水
 * @date 2020/04/16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "阿里云服务配置")
public class SysOss extends BaseEntity<Long> {

	private String operation;
	private String endPoint;
	private String accessKeyId;
	private String accessKeySecret;
	private String bucketName;
	private String homedir;

}
