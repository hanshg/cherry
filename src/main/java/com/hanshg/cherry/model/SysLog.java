package com.hanshg.cherry.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author 柠檬水
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "日志表")
public class SysLog extends BaseEntity<Long> {

    private String username;
    private String operation;
    @ApiModelProperty(value = "类型",example = "1")
    private Integer type;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String time;
    private String method;
    private String ip;
    private String params;
    private String borderName;
    private String osName;

    public interface Type {
        int LOGIN = 0; //登录
        int MENU = 1; //菜单
        int BUTTON = 2; //按钮
    }
}
