package com.hanshg.cherry.model;


import com.hanshg.cherry.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author 柠檬水
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "角色权限表")
public class RolePermission extends BaseEntity<Long> {

  @ApiModelProperty(value = "角色id",example = "1")
  private Long roleId;

  @ApiModelProperty(value = "权限id",example = "1")
  private Long permissionId;

}
