package com.hanshg.cherry.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

/**
 * @author hanshg
 * @date 2021/8/11
 */
@Configuration
public class BrowserSecurityConfig {


    @Autowired
    private DataSource dataSource;

    /*记住我操作*/
    @Bean
    public PersistentTokenRepository persistentTokenRepository() {

        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();   // 赋值数据源
        jdbcTokenRepository.setDataSource(dataSource);
        //jdbcTokenRepository.setCreateTableOnStartup(true);      //第一次启动的时候打开该注解进行建表来存放token
        return jdbcTokenRepository;

    }


}
