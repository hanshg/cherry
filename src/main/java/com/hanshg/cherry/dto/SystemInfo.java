package com.hanshg.cherry.dto;

import lombok.Data;

/**
 * @author 柠檬水
 */
@Data
public class SystemInfo {

    private String name;

    private String value;

}
