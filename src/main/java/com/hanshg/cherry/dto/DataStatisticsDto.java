package com.hanshg.cherry.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 柠檬水
 */
@Data
public class DataStatisticsDto {

    private String name;
    @ApiModelProperty(value = "总数量",example = "20")
    private Integer num;
}
