package com.hanshg.cherry.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author 柠檬水
 */
public class GenerateDetail implements Serializable {

	private String beanName;

	private List<BeanField> fields;

	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public List<BeanField> getFields() {
		return fields;
	}

	public void setFields(List<BeanField> fields) {
		this.fields = fields;
	}
}
