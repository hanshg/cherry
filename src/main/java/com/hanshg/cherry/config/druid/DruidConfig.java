package com.hanshg.cherry.config.druid;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

/**
 * 处理阿里云Druid连接池不生效问题
 * @author 柠檬水
 */
@Configuration
public class DruidConfig {

    @ConfigurationProperties("spring.datasource.druid")
    @Bean
    public DataSource dataSourceOne(){
        return DruidDataSourceBuilder.create().build();
    }

    /*
     * 解决druid 日志报错：discard long time none received connection:xxx
     * */
    @PostConstruct
    public void setProperties(){
        System.setProperty("druid.mysql.usePingMethod","false");
    }

}
