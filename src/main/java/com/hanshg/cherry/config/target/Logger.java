package com.hanshg.cherry.config.target;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 日志注解类
 * @author 柠檬水
 * @param
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Logger {

    /**
     * 属性值
     * @return
     */
    String value() default "";

    /**
     * 类型 0登录  1菜单  2按钮
     * @return
     */
    int type() default 0;
}
