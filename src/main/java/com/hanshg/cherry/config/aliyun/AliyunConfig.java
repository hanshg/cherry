package com.hanshg.cherry.config.aliyun;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 阿里云配置
 *
 * @author 柠檬水
 * @date 2020/04/16
 */
@Component
@ConfigurationProperties(prefix = "aliyun")
public class AliyunConfig {

    /**
     * 标识是否开启上传
     */
    private String flag;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
