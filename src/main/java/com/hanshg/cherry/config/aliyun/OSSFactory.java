package com.hanshg.cherry.config.aliyun;

import com.hanshg.cherry.model.SysOss;
import com.hanshg.cherry.service.aliyun.OssService;
import com.hanshg.cherry.util.spirng.SpringContextUtils;

/**
 * ossfactory
 *
 * @ClassName OSSFactory
 * @Description TODO
 * @Author 柠檬水
 * @Date 2020/4/16 15:39
 * @Version 1.0
 **/
public class OSSFactory {

    private static OssService ossService;

    static {
        OSSFactory.ossService = (OssService) SpringContextUtils.getBean("ossServiceImpl");
    }

    public static CloudStorageService build() {
        //获取云存储配置信息
        SysOss sysOss = ossService.getSysOss();
        if (null != sysOss) {
            return new AliyunCloudStorageService(sysOss);
        }
        return null;
    }
}
