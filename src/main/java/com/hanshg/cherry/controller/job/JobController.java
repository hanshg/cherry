package com.hanshg.cherry.controller.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.PageTableRequest;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.config.target.Logger;
import com.hanshg.cherry.model.SysJob;
import com.hanshg.cherry.service.job.JobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 工作的控制器
 *
 * @author 柠檬水
 * @date 2020/04/20
 */
@Controller
@RequestMapping("job")
@Slf4j
@Api(tags = "定时任务控制器")
public class JobController {

    private final JobService jobService;

    public JobController(JobService jobService) {
        this.jobService = jobService;
    }

    /**
     * 列表页面
     *
     * @param request 请求
     * @return {@link Results <SysJob>}
     */
    @GetMapping("/listPage")
    @ApiOperation(value = "分页列表")
    @PreAuthorize("hasAuthority('sys:job:query')")
    @ResponseBody
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @Logger(value = "查询定时任务列表", type = 2)
    public Results<SysJob> listPage(PageTableRequest request) {
        Page<SysJob> page = new Page<>(request.getPage(), request.getLimit());
        IPage<SysJob> jobPage = jobService.page(page);
        return Results.success((int) jobPage.getTotal(), jobPage.getRecords());
    }

    /**
     * 列表页面
     *
     * @param request 请求
     * @return {@link Results <SysJob>}
     */
    @GetMapping("/listPageByInfo")
    @ApiOperation(value = "分页模糊列表")
    @PreAuthorize("hasAuthority('sys:job:query')")
    @ResponseBody
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results<SysJob> listPageByInfo(PageTableRequest request, SysJob sysJob) {
        Page<SysJob> page = new Page<>(request.getPage(), request.getLimit());
        QueryWrapper<SysJob> wrapper = new QueryWrapper<>();
        wrapper.like("bean_name", sysJob.getBeanName());
        Page<SysJob> jobPage = jobService.page(page,wrapper);
        return Results.success((int) jobPage.getTotal(), jobPage.getRecords());
    }

    /**
     * 保存定时任务
     */
    @ApiOperation(value = "保存定时任务", notes = "跳转到保存定时任务页面")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @GetMapping(value = "/add")
    @PreAuthorize("hasAuthority('sys:job:add')")
    @Logger(value = "新增定时任务", type = 2)
    public String addJob(Model model) {
        model.addAttribute("sysJob", new SysJob());
        return "job/job-add";
    }

    /**
     * 编辑定时任务
     */
    @ApiOperation(value = "编辑定时任务", notes = "跳转到编辑定时任务页面")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @GetMapping(value = "/edit")
    @PreAuthorize("hasAuthority('sys:job:edit')")
    @Logger(value = "编辑定时任务", type = 2)
    public String editJob(Model model, SysJob sysJob) {
        model.addAttribute("sysJob", jobService.getById(sysJob.getId()));
        return "job/job-edit";
    }

    /**
     * 保存更新
     *
     * @return {@link Results <SysJob>}
     */
    @PostMapping("/saveJob")
    @ApiOperation(value = "保存更新定时任务")
    @ResponseBody
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results<SysJob> saveJob(SysJob sysJob) {
        return jobService.saveJob(sysJob);
    }

    /**
     * 保存更新
     *
     * @return {@link Results <SysJob>}
     */
    @PostMapping("/updateJob")
    @ApiOperation(value = "保存更新定时任务")
    @ResponseBody
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results<SysJob> updateJob(SysJob sysJob) {
        return jobService.updateJob(sysJob);
    }

    /**
     * 保存更新
     *
     * @return {@link Results <SysJob>}
     */
    @PostMapping("/mdJob")
    @ApiOperation(value = "启动禁用定时任务")
    @ResponseBody
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results<SysJob> mdJob(SysJob sysJob) {
        SysJob newSysJob = jobService.getById(sysJob.getId());
        newSysJob.setStatus(sysJob.getStatus());
        return jobService.updateJob(newSysJob);
    }

    /**
     * 删除任务
     *
     * @return {@link Results <SysJob>}
     */
    @GetMapping("/deleteJob")
    @ApiOperation(value = "删除定时任务")
    @PreAuthorize("hasAuthority('sys:job:del')")
    @ResponseBody
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @Logger(value = "删除定时任务", type = 2)
    public Results<SysJob> deleteJob(SysJob sysJob) {
        return jobService.deleteJob(sysJob);
    }
}
