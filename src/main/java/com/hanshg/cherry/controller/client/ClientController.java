package com.hanshg.cherry.controller.client;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.config.target.Logger;
import com.hanshg.cherry.model.SysClientMenu;
import com.hanshg.cherry.service.client.ClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 客户端控制器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@Controller
@RequestMapping("client")
@Slf4j
@Api(tags = "前台网站")
public class ClientController {

    @Autowired
    private ClientService clientService;

    /**
     * 指数
     *
     * @return {@link String}
     */
    @RequestMapping("/index")
    public String index() {
        return "client/client-index";
    }

    /**
     * 产品
     *
     * @return {@link String}
     */
    @RequestMapping("/product")
    public String product() {
        return "client/client-product";
    }

    /**
     * 新闻
     *
     * @return {@link String}
     */
    @RequestMapping("/news")
    public String news() {
        return "client/client-news";
    }

    /**
     * 例子
     *
     * @return {@link String}
     */
    @RequestMapping("/example")
    public String example() {
        return "client/client-example";
    }

    /**
     * 关于
     *
     * @return {@link String}
     */
    @RequestMapping("/about")
    public String about() {
        return "client/client-about";
    }

    /**
     * 新闻详细信息
     *
     * @return {@link String}
     */
    @RequestMapping("/newsDetail")
    public String newsDetail() {
        return "client/newsDetail";
    }


    /**
     * 得到所有
     *
     * @return {@link List<SysClientMenu>}
     */
    @GetMapping("/allMenu")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ResponseBody
    public List<SysClientMenu> getAll() {
        List<SysClientMenu> menuList = clientService.getAll();
        return menuList;
    }

    /**
     * 列表
     *
     * @return {@link Results<SysClientMenu>}
     */
    @GetMapping("/list")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ResponseBody
    public Results<SysClientMenu> list() {
        return clientService.list();
    }

    /**
     * 保存或更新
     *
     * @param clientMenu 客户端菜单
     * @return {@link Results}
     */
    @PostMapping("/saveOrUpdate")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ResponseBody
    public Results saveOrUpdate(SysClientMenu clientMenu){
        if (null != clientMenu.getId() && 0 != clientMenu.getId()) {
            return clientService.update(clientMenu);
        }
        return clientService.save(clientMenu);
    }

    /**
     * 客户端编辑
     *
     * @param model      模型
     * @param clientMenu 客户端菜单
     * @return {@link ModelAndView}
     */
    @ApiOperation(value = "编辑页面", notes = "跳转到菜单信息编辑页面")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @GetMapping(value = "/addOrEdit")
    @Logger(value = "新增编辑文件",type = 2)
    public ModelAndView clientEdit(Model model, SysClientMenu clientMenu) {
        SysClientMenu newClient = new SysClientMenu();
        if (0 != clientMenu.getId()) {
            newClient = clientService.getClientMenuById(clientMenu.getId());
        }
        model.addAttribute("clientMenu", newClient);
        ModelAndView modelAndView = new ModelAndView("manage/client-add");
        return modelAndView;
    }

    /**
     * 删除
     *
     * @param clientMenu 客户端菜单
     * @return {@link Results}
     */
    @GetMapping("/delete")
    @ResponseBody
    @ApiOperation(value = "删除信息", notes = "删除菜单信息")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ApiImplicitParam(name = "clientMenu", value = "菜单实体类", required = true)
    @Logger(value = "删除菜单",type = 2)
    public Results delete(SysClientMenu clientMenu) {
        SysClientMenu menu = clientService.getClientMenuById(clientMenu.getId());
        //更新状态为无效
        menu.setStatus(0);
        clientService.update(menu);
        int count = clientService.deleteById(clientMenu.getId());
        if (count > 0) {
            return Results.success();
        } else {
            return Results.failure();
        }
    }

    /**
     * 回收
     *
     * @return {@link String}
     */
    @ApiOperation(value = "回收站", notes = "跳转到回收站页面")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @GetMapping("/recycle")
    public String recycle(){
        return "manage/client-recycle";
    }

    /**
     * 回收列表
     *
     * @return {@link Results<SysClientMenu>}
     */
    @GetMapping("/recycleList")
    @ResponseBody
    public Results<SysClientMenu> recycleList() {
        return clientService.recycleList();
    }

    /**
     * 删除回收
     *
     * @param clientMenu 客户端菜单
     * @return {@link Results}
     */
    @GetMapping("/deleteRecycle")
    @ResponseBody
    @ApiOperation(value = "删除回收站信息", notes = "删除回收站信息")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results deleteRecycle(SysClientMenu clientMenu) {
        int count = clientService.deleteRecycle(clientMenu.getId());
        if (count > 0) {
            return Results.success();
        } else {
            return Results.failure();
        }
    }

    /**
     * 减少
     *
     * @param clientMenu 客户端菜单
     * @return {@link Results}
     */
    @GetMapping("/reduce")
    @ResponseBody
    @ApiOperation(value = "还原信息", notes = "还原回收站信息")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results reduce(SysClientMenu clientMenu) {
        return clientService.reduceMenu(clientMenu);
    }
}
