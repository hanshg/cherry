package com.hanshg.cherry.controller.generate;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.config.target.Logger;
import com.hanshg.cherry.dto.BeanField;
import com.hanshg.cherry.dto.GenerateDetail;
import com.hanshg.cherry.dto.GenerateInput;
import com.hanshg.cherry.service.GenerateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 代码生成接口
 * @author 柠檬水
 */
@Api(tags = "代码生成")
@RestController
@Slf4j
@RequestMapping("/generate")
public class GenerateController {

	@Autowired
	private GenerateService generateService;

	/**
	 * 生成的表名
	 *
	 * @param tableName 表名
	 * @return {@link GenerateDetail}
	 */
	@ApiOperation("根据表名显示表信息")
	@GetMapping(params = { "tableName" })
	@ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
	@Logger(value = "预览代码",type = 2)
	public GenerateDetail generateByTableName(String tableName) {
		GenerateDetail detail = new GenerateDetail();
		detail.setBeanName(generateService.upperFirstChar(tableName));
		List<BeanField> fields = generateService.listBeanField(tableName);
		detail.setFields(fields);

		return detail;
	}

	/**
	 * 保存
	 *
	 * @param input 输入
	 */
	@ApiOperation("生成代码")
	@PostMapping(value = "/save")
	@ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
	@ResponseBody
	@Logger(value = "生成代码",type = 2)
	public void save(@RequestBody GenerateInput input) {
		generateService.saveCode(input);
	}

}
