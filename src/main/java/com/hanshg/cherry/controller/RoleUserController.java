package com.hanshg.cherry.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.service.RoleUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 柠檬水
 */
@RestController
@RequestMapping("roleuser")
@Slf4j
@Api(tags = "用户角色控制层")
public class RoleUserController {

    @Autowired
    private RoleUserService  roleUserService;

    /**
     * 角色用户的用户id
     *
     * @param userId 用户id
     * @return {@link Results}
     */
    @PostMapping("/getRoleUserByUserId")
    @ApiOperation(value = "获取当前用户角色", notes = "获取当前用户角色")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ApiImplicitParam(name = "userId",value = "用户Id", required = true)
    public Results getRoleUserByUserId(Long userId){
        log.info("RoleUserController.getRoleUserByUserId() param="+userId);
        return roleUserService.getSysRoleUserByUserId(userId);
    }
}
