package com.hanshg.cherry.controller;

import com.alibaba.fastjson.JSONArray;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.config.target.Logger;
import com.hanshg.cherry.dto.RoleDto;
import com.hanshg.cherry.model.SysPermission;
import com.hanshg.cherry.service.PermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @author 柠檬水
 */
@Controller
@RequestMapping("permission")
@Slf4j
@Api(tags = "菜单权限控制层")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;


    /**
     * 列出所有权限
     *
     * @return {@link Results<JSONArray>}
     */
    @GetMapping(value = "/listAllPermission")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:menu:query')")
    @ApiOperation(value = "获取所有权限值返回JSON", notes = "获取所有菜单的权限值")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results<JSONArray> listAllPermission() {
        return permissionService.listAllPermission();
    }

    /**
     * 列出所有权限,角色id
     *
     * @param roleDto 角色dto
     * @return {@link Results<SysPermission>}
     */
    @GetMapping(value = "/listAllPermissionByRoleId")
    @ResponseBody
    @ApiOperation(value = "获取角色权限", notes = "根据角色Id去查询拥有的权限")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results<SysPermission> listAllPermissionByRoleId(RoleDto roleDto) {
        log.info(getClass().getName() + " : param =  " + roleDto);
        return permissionService.listByRoleId(roleDto.getId());
    }

    /**
     * 得到菜单
     *
     * @return {@link Results}
     */
    @GetMapping("/menuAll")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:menu:query')")
    @Logger(value = "查询菜单",type = 1)
    @ApiOperation(value = "获取所有权限值返回Object", notes = "获取所有菜单的权限值")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results getMenuAll(){
        return permissionService.getMenuAll();
    }

    /**
     * 添加权限
     *
     * @param model 模型
     * @return {@link String}
     */
    @GetMapping(value = "/add")
    @PreAuthorize("hasAuthority('sys:menu:add')")
    @ApiOperation(value = "新增页面", notes = "跳转到菜单信息新增页面")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @Logger(value = "新增菜单",type = 2)
    public String addPermission(Model model) {
        model.addAttribute("sysPermission",new SysPermission());
        return "permission/permission-add";
    }

    /**
     * 保存许可
     *
     * @param permission 许可
     * @return {@link Results<SysPermission>}
     */
    @PostMapping(value = "/add")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:menu:add')")
    @ApiOperation(value = "添加菜单", notes = "保存用户新增的菜单信息")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ApiImplicitParam(name = "sysPermission", value = "菜单权限实体sysPermission", required = true, dataType = "SysPermission")
    public Results<SysPermission> savePermission(@RequestBody SysPermission permission) {
        return permissionService.save(permission);
    }

    /**
     * 编辑权限
     *
     * @param model      模型
     * @param permission 许可
     * @return {@link String}
     */
    @GetMapping(value = "/edit")
    @ApiOperation(value = "编辑页面", notes = "跳转到菜单信息编辑页面")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @Logger(value = "编辑菜单",type = 2)
    public String editPermission(Model model, SysPermission permission) {
        model.addAttribute("sysPermission",permissionService.getSysPermissionById(permission.getId()));
        return "permission/permission-add";
    }

    /**
     * 更新许可
     *
     * @param permission 许可
     * @return {@link Results}
     */
    @PostMapping(value = "/edit")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:menu:edit')")
    @ApiOperation(value = "更新菜单信息", notes = "保存用户编辑完的菜单信息")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ApiImplicitParam(name = "sysPermission", value = "菜单权限实体sysPermission", required = true, dataType = "SysPermission")
    public Results updatePermission(@RequestBody  SysPermission permission) {
        return permissionService.updateSysPermission(permission);
    }

    /**
     * 删除权限
     *
     * @param sysPermission 系统权限
     * @return {@link Results}
     */
    @GetMapping(value = "/delete")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:menu:del')")
    @ApiOperation(value = "删除菜单", notes = "根据菜单Id去删除菜单")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @Logger(value = "删除菜单",type = 2)
    public Results deletePermission(SysPermission sysPermission) {
        return permissionService.delete(sysPermission.getId());
    }

    /**
     * 把菜单
     *
     * @param userId 用户id
     * @return {@link Results<SysPermission>}
     */
    @GetMapping(value = "/menu")
    @ResponseBody
    @ApiOperation(value = "获取菜单", notes = "获取用户所属角色下能显示的菜单")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ApiImplicitParam(name = "userId", value = "userId", required = true, dataType = "Long")
    public Results<SysPermission> getMenu(Long userId) {
        return permissionService.getMenu(userId);
    }

}
