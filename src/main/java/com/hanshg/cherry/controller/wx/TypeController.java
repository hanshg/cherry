package com.hanshg.cherry.controller.wx;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.JSONResult;
import com.hanshg.cherry.service.wx.TypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类型控制器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@RestController
@RequestMapping("client/type")
@Slf4j
@Api(tags = "新闻类型控制器")
public class TypeController {

    private final TypeService typeService;

    public TypeController(TypeService typeService) {
        this.typeService = typeService;
    }

    /**
     * 列出所有
     *
     * @return {@link JSONResult}
     */
    @GetMapping("/listAll")
    @ApiOperation(value = "获取所有类型列表")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public JSONResult listAll() {
        return typeService.getNewsType();
    }

}
