package com.hanshg.cherry.controller.wx;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.JSONResult;
import com.hanshg.cherry.service.wx.NewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 新闻控制器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@RestController
@RequestMapping("client/news")
@Slf4j
@Api(tags = "新闻资讯控制器")
public class NewsController {

    private final NewsService newsService;

    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    /**
     * 列表
     *
     * @param typeId id类型
     * @return {@link JSONResult}
     */
    @GetMapping("/list")
    @ApiOperation(value = "根据新闻类型获取新闻列表")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public JSONResult list(Long typeId) {
        log.info("访问新闻信息数据"+typeId);
        return newsService.getNewsByTypeId(typeId);
    }

    /**
     * 新闻详细信息
     *
     * @param newsId 消息id
     * @return {@link JSONResult}
     */
    @GetMapping("/detail/{newsId}")
    @ApiOperation(value = "获取新闻详情")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public JSONResult newsDetails(@PathVariable Long newsId) {
        log.info("根据新闻id访问了数据"+newsId);
        return newsService.getNewsById(newsId);
    }
}
