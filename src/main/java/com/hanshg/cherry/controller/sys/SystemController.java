package com.hanshg.cherry.controller.sys;

import com.hanshg.cherry.dto.DataStatisticsDto;
import com.hanshg.cherry.dto.SystemInfo;
import com.hanshg.cherry.mapper.PermissionMapper;
import com.hanshg.cherry.mapper.RoleMapper;
import com.hanshg.cherry.mapper.UserMapper;
import com.hanshg.cherry.mapper.log.LogMapper;
import com.hanshg.cherry.mapper.sys.DataTypeMapper;
import com.hanshg.cherry.service.client.ClientService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统控制器
 *
 * @author 柠檬水
 * @date 2020/04/13
 */
@RestController
@RequestMapping("/sys")
@Slf4j
@Api(tags = "获取系统信息及数据统计")
public class SystemController {

    private final ClientService clientService;

    private final UserMapper userMapper;

    private final RoleMapper roleMapper;

    private final PermissionMapper permissionMapper;

    private final DataTypeMapper dataTypeMapper;

    private final LogMapper logMapper;

    public SystemController(ClientService clientService, UserMapper userMapper, RoleMapper roleMapper, PermissionMapper permissionMapper, DataTypeMapper dataTypeMapper, LogMapper logMapper) {
        this.clientService = clientService;
        this.userMapper = userMapper;
        this.roleMapper = roleMapper;
        this.permissionMapper = permissionMapper;
        this.dataTypeMapper = dataTypeMapper;
        this.logMapper = logMapper;
    }


    @RequestMapping("/welcome")
    public ModelAndView getPage(ModelAndView modelAndView, String pageName){
        modelAndView.addObject("system",clientService.server());
        log.info("获取系统信息成功！");
        modelAndView.setViewName(pageName);
        return modelAndView;
    }

    @GetMapping("/sysInfo")
    public List<SystemInfo> sysInfo(){
        List<SystemInfo> list = new ArrayList<>();
        SystemInfo systemInfo = null;

        systemInfo = new SystemInfo();
        systemInfo.setName("操作系统名称");
        systemInfo.setValue(System.getProperty("os.name"));
        list.add(systemInfo);

        systemInfo = new SystemInfo();
        systemInfo.setName("操作系统的架构");
        systemInfo.setValue(System.getProperty("os.arch"));
        list.add(systemInfo);

        systemInfo = new SystemInfo();
        systemInfo.setName("操作系统版本");
        systemInfo.setValue(System.getProperty("os.version"));
        list.add(systemInfo);

        systemInfo = new SystemInfo();
        systemInfo.setName("服务器地址");
        systemInfo.setValue("https://gitee.com/hanshg/cherry");
        list.add(systemInfo);

        systemInfo = new SystemInfo();
        systemInfo.setName("Java版本");
        systemInfo.setValue(System.getProperty("java.version"));
        list.add(systemInfo);

        systemInfo = new SystemInfo();
        systemInfo.setName("Java安装目录");
        systemInfo.setValue(System.getProperty("java.home"));
        list.add(systemInfo);

        systemInfo = new SystemInfo();
        systemInfo.setName("Java虚拟机实现名称");
        systemInfo.setValue(System.getProperty("java.vm.name"));
        list.add(systemInfo);

        systemInfo = new SystemInfo();
        systemInfo.setName("Java虚拟机实现版本");
        systemInfo.setValue(System.getProperty("java.vm.version"));
        list.add(systemInfo);

        systemInfo = new SystemInfo();
        systemInfo.setName("Java运行时环境规范名称");
        systemInfo.setValue(System.getProperty("java.specification.name"));
        list.add(systemInfo);

        systemInfo = new SystemInfo();
        systemInfo.setName("使用数据库");
        systemInfo.setValue(dataTypeMapper.dataBase());
        list.add(systemInfo);

        systemInfo = new SystemInfo();
        systemInfo.setName("数据库版本");
        systemInfo.setValue(dataTypeMapper.mysqlVersion());
        list.add(systemInfo);
        return list;
    }

    @GetMapping("/sysData")
    public List<DataStatisticsDto> sysData(){
        List<DataStatisticsDto> list = new ArrayList<>();
        DataStatisticsDto dto = null;
        dto = new DataStatisticsDto();
        dto.setName("用户数");
        dto.setNum(userMapper.countUser());
        list.add(dto);

        dto = new DataStatisticsDto();
        dto.setName("角色数");
        dto.setNum(roleMapper.countAllRoles());
        list.add(dto);

        dto = new DataStatisticsDto();
        dto.setName("菜单数");
        dto.setNum(permissionMapper.countMenu());
        list.add(dto);

        dto = new DataStatisticsDto();
        dto.setName("按钮数");
        dto.setNum(permissionMapper.countButton());
        list.add(dto);

        dto = new DataStatisticsDto();
        dto.setName("日志数");
        dto.setNum(Math.toIntExact(logMapper.selectCount(null)));
        list.add(dto);

        dto = new DataStatisticsDto();
        dto.setName("数据表数");
        dto.setNum(dataTypeMapper.countTable());
        list.add(dto);

        return list;
    }

}
