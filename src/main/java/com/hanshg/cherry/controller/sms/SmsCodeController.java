package com.hanshg.cherry.controller.sms;

import com.alibaba.fastjson.JSONObject;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.core.Constants;
import com.hanshg.cherry.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author hanshg
 * @date 2021/8/11
 */
@Controller
@Slf4j
@RequestMapping("sms")
@Api(tags = "短信验证登录接口")
public class SmsCodeController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/code/{mobile}")
    @ResponseBody
    @ApiOperation(value = "登录短信验证码", notes = "登录短信验证码")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public JSONObject smsCode(@PathVariable String mobile) {
        return userService.sendSms(mobile, Constants.SMS_CODE);
    }

}
