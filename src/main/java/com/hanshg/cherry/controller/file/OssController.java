package com.hanshg.cherry.controller.file;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.config.target.Logger;
import com.hanshg.cherry.model.SysOss;
import com.hanshg.cherry.service.aliyun.OssService;
import com.hanshg.cherry.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 阿里云oss控制器
 *
 * @ClassName OssController
 * @Description oss控制器
 * @Author 柠檬水
 * @Date 2020/4/16 9:54
 * @Version 1.0
 **/
@Controller
@RequestMapping("aliyun")
@Slf4j
@Api(tags = "oss控制器")
public class OssController {

    @Autowired
    private OssService ossService;

    @Autowired
    private UserService userService;

    /**
     * 配置阿里云服务器
     *
     * @param model 模型
     * @return {@link String}
     */
    @GetMapping(value = "/config")
    @ApiOperation(value = "进入加载")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @Logger(value = "配置阿里云服务器", type = 1)
    public String list(Model model) {
        SysOss sysOss = ossService.getSysOss();
        if (sysOss == null) {
            sysOss = new SysOss();
        }
        model.addAttribute("sysOss", sysOss);
        return "file/server-set";
    }


    /**
     * 保存或更新
     *
     * @param sysOss 系统邮件
     * @return {@link Results}
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "保存更新阿里云oss配置")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ResponseBody
    public Results saveOrUpdate(SysOss sysOss) {
        sysOss.setOperation(userService.getLoginUser().getUsername());
        boolean b = ossService.saveOrUpdate(sysOss);
        if (b) {
            return Results.success();
        }
        return Results.failure();
    }
}
