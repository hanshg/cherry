package com.hanshg.cherry.controller.file;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.PageTableRequest;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.config.target.Logger;
import com.hanshg.cherry.dto.SysMailInfoDto;
import com.hanshg.cherry.model.SysMail;
import com.hanshg.cherry.model.SysMailInfo;
import com.hanshg.cherry.service.mail.MailInfoService;
import com.hanshg.cherry.service.mail.MailService;
import com.hanshg.cherry.service.mail.SysMailService;
import com.hanshg.cherry.util.core.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 柠檬水
 */
@Controller
@RequestMapping("mail")
@Slf4j
@Api(tags = "邮件发送控制层")
public class MailController {

    /**
     * 处理邮件服务器设置
     */
    @Autowired
    private SysMailService sysMailService;

    /**
     * 邮件发送工具类
     */
    @Autowired
    private MailService mailService;

    /**
     * 邮件信息查询
     */
    @Autowired
    private MailInfoService mailInfoService;

    /**
     * 保存或更新
     *
     * @param sysMail 系统邮件
     * @return {@link Results}
     */
    @PostMapping(value = "/saveOrUpdate")
    @ApiOperation(value = "保存邮件配置")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ResponseBody
    public Results saveOrUpdate(SysMail sysMail) {
        if (null != sysMail.getId() && 0 != sysMail.getId()) {
            return sysMailService.updateMail(sysMail);
        }
        return sysMailService.saveMail(sysMail);
    }

    /**
     * 加载
     *
     * @param model 模型
     * @return {@link String}
     */
    @GetMapping(value = "/config")
    @ApiOperation(value = "进入加载")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @Logger(value = "配置邮件服务器", type = 1)
    public String list(Model model) {
        SysMail sysMail = sysMailService.getSysMail();
        if (sysMail == null) {
            sysMail = new SysMail();
        }
        model.addAttribute("sysMail", sysMail);
        return "mail/mail-set";
    }

    /**
     * 发送邮件
     *
     * @param mailInfo 邮件信息
     * @return {@link Results}
     */
    @PostMapping("/send")
    @ApiOperation(value = "发送邮件")
    @PreAuthorize("hasAuthority('sys:mail:send')")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ResponseBody
    public Results sendMail(SysMailInfo mailInfo) {
        try {
            List<Map<String, String>> acts = new ArrayList<>();
            //发送人
            String[] address = mailInfo.getAddress().split(";");
            if (!StringUtils.isEmpty(mailInfo.getAnnex())) {
                Map<String, String> hashMap = new HashMap<>();
                hashMap.put("annex", mailInfo.getAnnex());
                hashMap.put("fileName", mailInfo.getFileName());
                //附件
                acts.add(hashMap);
            }

            //抄送人
            String[] ccs = null;
            if (!StringUtils.isEmpty(mailInfo.getCc())) {
                ccs = mailInfo.getCc().split(";");
            }
            //isNet为true代表附件信息将在http请求中获取
            mailService.sendSimpleTextMailActual(mailInfo.getTitle(), mailInfo.getContent(), address, ccs, null, acts, true);
        } catch (Exception e) {
            e.printStackTrace();
            return Results.failure(550, "邮件发送失败，请联系柠檬水小哥哥！");
        }
        //发送完成保存一份
        return mailInfoService.saveMailInfo(mailInfo);
    }

    /**
     * 列表
     *
     * @param request 请求
     * @return {@link Results<SysMailInfo>}
     */
    @GetMapping("/listPage")
    @ApiOperation(value = "分页列表列表")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ResponseBody
    public Results<SysMailInfo> list(PageTableRequest request) {
        Results<SysMailInfo> list = mailInfoService.getMailInfoByPage(request.getPage(), request.getLimit());
        return list;
    }

    /**
     * 列表页
     *
     * @param request  请求
     * @param mailInfo 邮件信息
     * @return {@link Results<SysMailInfo>}
     */
    @GetMapping("/listByPage")
    @ApiOperation(value = "模糊查询列表")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ResponseBody
    public Results<SysMailInfo> listByPage(PageTableRequest request, SysMailInfo mailInfo) {
        return mailInfoService.getMailInfoByPage(request.getPage(), request.getLimit(), mailInfo);
    }

    /**
     * 删除
     *
     * @param id id
     * @return {@link Results}
     */
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ApiOperation(value = "删除")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ResponseBody
    public Results delete(Long id) {
        return mailInfoService.deleteMailInfo(id);
    }

    /**
     * 添加
     *
     * @param model 模型
     * @return {@link String}
     */
    @GetMapping(value = "/mail-send")
    @ApiOperation(value = "写邮件")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public String add(Model model) {
        SysMailInfoDto mailInfo = new SysMailInfoDto();
        model.addAttribute("sysMailInfo", mailInfo);
        return "mail/mail-send";
    }

    /**
     * 信息
     *
     * @param model    模型
     * @param mailInfo 邮件信息
     * @return {@link String}
     */
    @GetMapping(value = "/info")
    @ApiOperation(value = "查看邮件")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public String info(Model model, SysMailInfo mailInfo) {
        model.addAttribute("sysMailInfo", mailInfoService.getMailInfoById(mailInfo.getId()));
        return "mail/mail-info";
    }

    /**
     * 删除回收
     *
     * @param sysMailInfo 系统邮件信息
     * @return {@link Results}
     */
    @GetMapping("/deleteRecycle")
    @ResponseBody
    @ApiOperation(value = "删除回收站信息", notes = "删除回收站信息")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results deleteRecycle(SysMailInfo sysMailInfo) {
        int count = mailInfoService.deleteRecycle(sysMailInfo.getId());
        if (count > 0) {
            return Results.success();
        } else {
            return Results.failure();
        }
    }

    /**
     * 回收
     *
     * @return {@link String}
     */
    @ApiOperation(value = "回收站", notes = "跳转到回收站页面")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @GetMapping("/recycle")
    public String recycle() {
        return "mail/mail-recycle";
    }

    /**
     * 减少
     *
     * @param sysMailInfo 系统邮件信息
     * @return {@link Results}
     */
    @GetMapping("/reduce")
    @ResponseBody
    @ApiOperation(value = "还原信息", notes = "还原回收站信息")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results reduce(SysMailInfo sysMailInfo) {
        return mailInfoService.reduceProduct(sysMailInfo);
    }

    /**
     * 回收列表
     *
     * @return {@link Results<SysMailInfo>}
     */
    @GetMapping("/recycleList")
    @ApiOperation(value = "回收站列表查询")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ResponseBody
    public Results<SysMailInfo> recycleList() {
        return mailInfoService.recycleList();
    }

}
