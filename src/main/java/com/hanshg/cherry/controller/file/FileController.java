package com.hanshg.cherry.controller.file;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hanshg.cherry.base.result.PageTableRequest;
import com.hanshg.cherry.base.result.Results;
import com.hanshg.cherry.config.target.Logger;
import com.hanshg.cherry.model.SysFile;
import com.hanshg.cherry.service.file.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author 柠檬水
 */
@Controller
@RequestMapping("/file")
@Slf4j
@Api(tags = "文件管理控制层")
public class FileController {

    @Autowired
    private FileService fileService;

    /**
     * 保存
     *
     * @param sysFile sys文件
     * @return {@link Results}
     */
    @PostMapping(value = "/add")
    @ApiOperation(value = "保存")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:file:add')")
    public Results save(SysFile sysFile) {
        log.info("RoleController.getAllRoles()");
        if (null != sysFile.getId() && 0 != sysFile.getId()) {
            return fileService.updateFile(sysFile);
        }
        return fileService.saveFile(sysFile);
    }

    /**
     * 通过id获取文件
     *
     * @param id id
     * @return {@link Results}
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "根据id获取")
    @PreAuthorize("hasAuthority('sys:file:query')")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results getFilesById(@PathVariable Long id) {
        return fileService.getFileById(id);
    }

    /**
     * 更新
     *
     * @param sysFile sys文件
     * @return {@link Results}
     */
    @PostMapping(value = "/edit")
    @ApiOperation(value = "修改")
    @PreAuthorize("hasAuthority('sys:file:edit')")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    public Results update(SysFile sysFile) {
        return fileService.updateFile(sysFile);
    }

    /**
     * 列表页面
     *
     * @param request 请求
     * @return {@link Results<SysFile>}
     */
    @GetMapping("/listPage")
    @ApiOperation(value = "列表")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:file:query')")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @Logger(value = "查询文件",type = 2)
    public Results<SysFile> listPage(PageTableRequest request) {
        request.countOffset();
        return fileService.getAllFileByPage(request.getOffset(), request.getLimit());
    }

    /**
     * 找到文件标题
     *
     * @param request 请求
     * @param title   标题
     * @return {@link Results<SysFile>}
     */
    @GetMapping("/findFileByTitle")
    @ApiOperation(value = "模糊查询文件")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:file:query')")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @Logger(value = "搜索查询文件",type = 2)
    public Results<SysFile> findFileByTitle(PageTableRequest request,String title) {
        request.countOffset();
        return fileService.findFileByTitle(title,request.getOffset(), request.getLimit());
    }

    /**
     * 删除
     *
     * @param sysFile sys文件
     * @return {@link Results}
     */
    @GetMapping(value = "/delete")
    @ApiOperation(value = "删除")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:file:del')")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @Logger(value = "删除文件",type = 2)
    public Results delete(SysFile sysFile) {
        return fileService.deleteFile(sysFile.getId());
    }

    /**
     * 角色编辑
     *
     * @param model   模型
     * @param sysFile sys文件
     * @return {@link ModelAndView}
     */
    @ApiOperation(value = "编辑页面", notes = "跳转到菜单信息编辑页面")
    @ApiOperationSupport(author = "云易智创 cloudeasy@cherryvip.cn")
    @GetMapping(value = "/addOrEdit")
    @PreAuthorize("hasAuthority('sys:file:edit') or hasAuthority('sys:file:add')")
    @Logger(value = "新增编辑文件",type = 2)
    public ModelAndView roleEdit(Model model, SysFile sysFile) {
        SysFile newSysFile = new SysFile();
        if (0 != sysFile.getId()) {
            newSysFile = fileService.getSysFileById(sysFile.getId());
        }
        model.addAttribute("sysFile", newSysFile);
        ModelAndView modelAndView = new ModelAndView("file/file-add");
        return modelAndView;
    }
}
