//  菜单显示异常修改tinymce/skins/ui/oxide/skin.min.css:96 .tox-silver-sink的z-index值
//  http://tinymce.ax-z.cn/   中文文档
layui.define(['jquery'], function (exports) {
    var $ = layui.$;
    var modFile = layui.cache.modules['tinymce'];
    var modPath = modFile.substr(0, modFile.lastIndexOf('.'));

    //  ----------------  以上代码无需修改  ----------------
    var settings = {
        base_url: modPath
        , images_upload_url: '/fileUpload/upload'//图片上传接口，可在option传入，也可在这里修改，option的值优先
        , language: 'zh_CN'//语言，可在option传入，也可在这里修改，option的值优先
        , success: function (res, succFun, failFun) {//图片上传完成回调 根据自己需要修改
            if (res.code == 0) {
                succFun(res.data.src);
            } else {
                failFun("网络错误：" + res.status);
            }
        }
    };
    //  ----------------  以下代码无需修改  ----------------
    var t = {};
    //初始化
    t.render = function (option, callback) {
        var admin = layui.admin || {};
        option.base_url = option.base_url ? option.base_url : settings.base_url;
        option.language = option.language ? option.language : settings.language;
        option.selector = option.selector ? option.selector : option.elem;
        option.quickbars_selection_toolbar = option.quickbars_selection_toolbar ? option.quickbars_selection_toolbar : 'cut copy | bold italic underline strikethrough ';
        option.plugins = option.plugins ? option.plugins : 'quickbars print preview searchreplace autolink fullscreen image link media codesample table code charmap hr advlist lists wordcount imagetools help';
        option.toolbar = option.toolbar ? option.toolbar : 'code undo redo | forecolor backcolor bold italic underline strikethrough | alignleft aligncenter alignright alignjustify outdent indent | bullist numlist image table codesample | link media | formatselect fontselect fontsizeselect ';
        option.fontsize_formats = option.fontsize_formats ? option.fontsize_formats : '8px 10px 12px 14px 16px 18px 24px 36px 48px 56px 72px';
        option.font_formats = option.font_formats ? option.font_formats : '微软雅黑=Microsoft YaHei,Helvetica Neue,PingFang SC,sans-serif;苹果苹方=PingFang SC,Microsoft YaHei,sans-serif;宋体=simsun,serif;仿宋体=FangSong,serif;黑体=SimHei,sans-serif;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats;知乎配置=BlinkMacSystemFont, Helvetica Neue, PingFang SC, Microsoft YaHei, Source Han Sans SC, Noto Sans CJK SC, WenQuanYi Micro Hei, sans-serif;小米配置=Helvetica Neue,Helvetica,Arial,Microsoft Yahei,Hiragino Sans GB,Heiti SC,WenQuanYi Micro Hei,sans-serif';
        option.resize = false;
        option.elementpath = false;
        option.branding = false;
        option.contextmenu_never_use_native = true;
        option.file_picker_types = option.file_picker_types ? option.file_picker_types : 'file image media'
        option.menubar = option.menubar ? option.menubar : 'file edit insert format table help';
        option.images_upload_url = option.images_upload_url ? option.images_upload_url : settings.images_upload_url;
        option.images_upload_handler = option.images_upload_handler ? option.images_upload_handler : function (blobInfo, succFun, failFun) {
            var formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            var ajaxOpt = {
                url: option.images_upload_url,
                dataType: 'json',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (res) {
                    settings.success(res, succFun, failFun)
                },
                error: function (res) {
                    failFun("网络错误：" + res.status);
                }
            };
            if (typeof admin.req == 'function') {
                admin.req(ajaxOpt);
            } else {
                $.ajax(ajaxOpt);
            }
        }
        option.menu = option.menu ? option.menu : {
            file: {title: '文件', items: 'newdocument | print preview fullscreen | wordcount'},
            edit: {title: '编辑', items: 'undo redo | cut copy paste pastetext selectall | searchreplace'},
            format: {
                title: '格式',
                items: 'bold italic underline strikethrough superscript subscript | formats | forecolor backcolor | removeformat'
            },
            table: {title: '表格', items: 'inserttable tableprops deletetable | cell row column'},
        };
        if (typeof tinymce == 'undefined') {
            $.ajax({//获取插件
                url: option.base_url + '/tinymce.js',
                dataType: 'script',
                cache: true,
                async: false,
            });
        }
        layui.sessionData('layui-tinymce', {
            key: option.selector,
            value: option
        })
        tinymce.init(option);
        if (typeof callback == 'function') {
            callback.call(option)
        }
        return tinymce.activeEditor;
    };
    t.init = t.render
    // 获取ID对应的编辑器对象
    t.get = (elem) => {
        if (elem && /^#|\./.test(elem)) {
            var id = elem.substr(1)
            var edit = tinymce.editors[id];
            if (!edit) {
                return console.error("编辑器未加载")
            }
            return edit
        } else {
            return console.error("elem错误")
        }
    }
    //重载
    t.reload = (option, callback) => {
        option = option || {}
        var edit = t.get(option.elem);
        var optionCache = layui.sessionData('layui-tinymce')[option.elem]
        edit.destroy()
        $.extend(optionCache, option)
        tinymce.init(optionCache)
        if (typeof callback == 'function') {
            callback.call(optionCache)
        }
        return tinymce.activeEditor;
    }
    exports('tinymce', t);
});
