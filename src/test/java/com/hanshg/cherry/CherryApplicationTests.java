package com.hanshg.cherry;

import com.hanshg.cherry.config.aliyun.AliyunConfig;
import com.hanshg.cherry.service.UserService;
import com.hanshg.cherry.service.aliyun.AliYunOssService;
import com.hanshg.cherry.service.mail.MailService;
import com.hanshg.cherry.service.oauth.TencentLoginService;
import com.hanshg.cherry.util.redis.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
class CherryApplicationTests {

    @Autowired
    private AliyunConfig aliyunConfig;

    @Autowired
    private TencentLoginService tencentLoginService;

    @Autowired
    private MailService mailService;

    @Autowired
    private AliYunOssService aliYunOssService;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private RedisUtil redisUtil;

//    @Resource
//    StringEncryptor encryptor;

    /**
     * 测试密码加密
     */
    @Test
    void contextLoads() {
        //System.out.println(new BCryptPasswordEncoder().encode("user"));
    }

    @Test
    void redisByName() {
//        redisUtil.flushDB();
//        redisUtil.set("name", 38);
//       System.out.println(redisUtil.get(Constants.LOGIN_VERIFY_SESSION));
    }

    @Test
    void dateTest(){
        //System.out.println(DateUtils.getDate());
    }


    @Test
    void getPass() {
//        String url = encryptor.encrypt("jdbc:mysql://cherrydb.mysql.rds.aliyuncs.com:3306/cherry?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&failOverReadOnly=false&serverTimezone=Asia/Shanghai");
//        String name = encryptor.encrypt("cherry");
//        String password = encryptor.encrypt("cherry@1234");
//        String redisUrl = encryptor.encrypt("cherry-r.redis.rds.aliyuncs.com");
//        System.out.println(url);
//        System.out.println(name);
//        System.out.println(password);
//        System.out.println(redisUrl);
    }

    @Test
    void wxLogin() {
        String url = tencentLoginService.wxLoginUrl();
        System.out.print(url);
    }

}
